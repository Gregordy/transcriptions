# Anna Akana - Things Girls Should Shut Up About

Listen up ladies!

I am tired of hearing certain things , coming out of your beautifuls mouths,
and I need you to shup up about them forever.

## 1 : Stop saying self-deprecating things about yourself 

I don't tolerate other people talking sh!t bout my friends, so that include you
talking sh!t about my friends!

So the next time you find yourself saying:  
I am so stupid  
Uhhg, I am so uglyyyy   
I'm just like a horrible person  

You better shut that beautiful face and start treating yourself with the same
courtesy and respect that you treat everyone else with or else we are going to
have problem.

Stop dissing your appearance and your intelligence   
Stop dismissing your talents and your achievements  

And stop putting down and doubting in who you are as a human being.  
Because at this point it's starting to insult the people who love you, a.k.a me.

## 2 : Stop defending the person who hurt you!

You are tempted to make excuses, for someone else's poor behaviour:  
He was just really stressed  
I know, but she doesn't mean it  
They were really just in a really tough spot.

Keep your amazing mouth **SHUT**.  
If someone else treated you badly, that's on them.

Look, I know you're a compassioning goddess with real emotional intelligence and
that therefore understand that this person hurting you is just an attempt for
this person to dissolve their own cure and/or a projection of their pain onto
you is human being but that doesn't excuse the fact that they hurt you.

Everyone gets to make their own choices and it's time to stop bending over
backwards to accomodate those who have made the choice that you are not worth it
to them.

Cause at this point it's insulting to people who love you, a.k.a you!

## 3: Stop being judgmental about other women for the sake of your ego's love of drama and chaos.

We do not need women policing putting down or punishing other women.

As long as the woman in question is not harming others or herself, we should not
be passing judgment on her actions.

So put your perfectly gloss lips together and keep them that way, unless you're
going to give this woman a YAS!

I'm Anna Akana, thank you for coming to my TED talk and thank you to Squarespace
for sponsoring today's video!

Le reste, c'est l'explication de son sponsor, voilà !!! : ] 